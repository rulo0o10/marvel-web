import React, { useEffect, useState } from "react";
import { Home, hashKey, DetailComic } from "./components";
import axios from "axios";
import Credentials from "./credentials/credentials";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "./style.scss";
function App() {
  const [comics, setComics] = useState([]);
  const [selectComic, setSelect] = React.useState({});
  const ts = new Date().getTime();
  let params = {
    ts: ts,
    apikey: Credentials.public_key,
    hash: hashKey(ts, Credentials)
  };
  useEffect(() => {
    axios("http://gateway.marvel.com/v1/public/comics", {
      params: params
    }).then(res => {
      setComics(() =>
        res.data.data.results.sort((a, b) =>
          a.images.length > b.images.length ? -1 : 1
        )
      );
    });
  }, []);
  return (
    <Router>
      <div className="container">
        <Switch>
          <Route path={"/comic-detail/:id"}>
            <DetailComic comic={selectComic} />
          </Route>
          <Route path="/">
            <Home select={setSelect} comics={comics} />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
