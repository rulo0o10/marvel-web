import React from "react";
import md5 from "md5";
import styled from "styled-components";
function hashKey(ts, credentials) {
  return md5(`${ts}${credentials.private_key}${credentials.public_key}`);
}

const Paginator = props => {
  const { quantity, active, handleActive, offset } = props;
  var FontAwesome = require("react-fontawesome");
  const lastPage = Number(quantity / offset);
  let items = [];
  for (let i = 0; i <= lastPage; i++) {
    items.push(
      <Item key={i} active={active} handleActive={handleActive}>
        {i + 1}
      </Item>
    );
  }

  return (
    <div className="paginator">
      <div
        className="itemPag arrows"
        style={{ visibility: active == 1 ? "hidden" : "visible" }}
        onClick={() => handleActive(active - 1)}
      >
        <FontAwesome
          className="super-crazy-colors"
          name="chevron-left"
          style={{
            cursor: "pointer"
          }}
        ></FontAwesome>
      </div>
      {items.length > 0 && items.map(I => I)}
      <div
        className="itemPag arrows"
        style={{ visibility: active < lastPage ? "visible" : "hidden" }}
        onClick={() => handleActive(active + 1)}
      >
        <FontAwesome
          className="super-crazy-colors"
          name="chevron-right"
          style={{
            cursor: "pointer"
          }}
        ></FontAwesome>
      </div>
    </div>
  );
};

const Item = ({ children, active, handleActive }) => {
  return (
    <div
      className={active == children ? "itemPag active" : "itemPag"}
      onClick={() => handleActive(children)}
    >
      {children}
    </div>
  );
};

const Loading = () => {
  return <StyledLoad>LOADING ...</StyledLoad>;
};

const StyledLoad = styled.div`
  background: white;
  display: flex;
  width: 100%;
  position: fixed;
  font-size: 1.5em;
  height: 100%;
  z-index: 10;
  justify-content: center;
  align-items: center;
  animation: 2s fadeOut infinite;
  @keyframes fadeOut {
    from {
      opacity: 1;
    }

    to {
      opacity: 0;
    }
  }
`;
export { hashKey, Paginator, Loading };
