import Home from "./home";
import { hashKey, Paginator } from "./util";
import  DetailComic  from "./detail";
export { Home, hashKey, Paginator, DetailComic };
