import React, { useEffect, useState, memo } from "react";

import { Paginator, Loading } from "./util";
import styled from "styled-components";
import { withRouter } from "react-router-dom";

const Home = props => {
  const { comics } = props;
  const [Activecomics, setActiveComics] = useState([]);
  const [activePage, setActivePage] = useState(1);
  const offset = 12;

  const changeItemPaginator = currentPage => {
    const total = (currentPage - 1) * offset; //offset harcode
    const rest = comics.slice(total);
    setActivePage(currentPage);
    setActiveComics([]);
    for (let i in rest) {
      if (Number(i) < offset) {
        setActiveComics(e => [...e, rest[i]]);
      }
    }
  };

  useEffect(() => {
    changeItemPaginator(1);
  }, [comics]);

  const goToDetail = id => {
    props.select(id);
    props.history.push("/comic-detail/" + id.id);
  };
  return Activecomics.length > 0 ? (
    <>
      <div className="header">
        MARVEL<span>API Explorer</span>
      </div>
      <div className="body-comics">
        <div className="wrapper-items">
          {Activecomics.length > 0 &&
            Activecomics.map((C, index) => (
              <ComicItem key={index} onClick={() => goToDetail(C)}>
                {C.images.length > 0 && (
                  <img
                    src={`${C.images[0]?.path}.${C.images[0]?.extension}`}
                  ></img>
                )}
                <Detail comic={C}></Detail>
              </ComicItem>
            ))}
        </div>
        <Paginator
          className="paginator"
          quantity={comics?.length}
          active={activePage}
          offset={offset}
          handleActive={i => changeItemPaginator(i)}
        ></Paginator>
      </div>
    </>
  ) : (
    <Loading></Loading>
  );
};

const ComicItem = styled.div`
  background: #c20927;
  border-radius: 4px;
  position: relative;
  overflow: hidden;
  cursor: pointer;
  box-shadow:-3px 1px 10px black;
  &:hover .info-comic {
    opacity: 1;
    height: 100%;
  }
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
    border-radius: 4px;
  }
  &::after {
    content: "";
    height: 60%;
    width: 100%;
    position: absolute;
    background: #fff5f51c;
    z-index: 1;
    left: 0;
    transform: skewY(-10deg);
    top: -20px;
  }
  .info-comic {
    transition: 0.6s all;
    background: #000000c7;
    position: absolute;
    width: 100%;
    height: 0;
    z-index: 10;
    opacity: 0;
    top: 0;
    display: flex;
    font-size: 1.2em;
    align-items: center;
    justify-content: center;
    text-align: center;
  }
`;

const Detail = ({ comic }) => {
  return <div className="info-comic">{comic.title}</div>;
};
export default withRouter(memo(Home));
