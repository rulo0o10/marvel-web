import React from "react";
import { withRouter } from "react-router-dom";

const DetailComic = props => {
  var FontAwesome = require("react-fontawesome");
  const {
    comic,
    comic: { images }
  } = props;

  const image =
    images && images.length > 0 && `${images[0].path}.${images[0].extension}`;

  if (Object.entries(comic).length == 0) props.history.push("/");

  return (
    <div className="detail-wrapper">
      <div className="image-section">
        <div
          className="wallpaper"
          style={{ background: image ? `url(${image})` : "#c20927" }}
        ></div>
        {image && <img width="400" heigth="400" src={image}></img>}
      </div>

      <div className="info-comic">
        <h2>{comic.title}</h2>
        <div className="detail">
          <div>Creator</div>
          <FontAwesome
            className="super-crazy-colors"
            name="chevron-left"
            style={{
              position: "fixed",
              left: "10px",
              top: "10px",
              color: "white",
              cursor: "pointer"
            }}
            onClick={() => props.history.push("/")}
          />

          <ul>
            {comic?.creators?.items?.map((c, i) => (
              <li key={i}>{c.name}</li>
            ))}
            {comic?.creators?.items?.length == 0 && <li>without creators</li>}
          </ul>

          <div> Stories</div>
          <ul>
            {comic?.stories?.items?.map((c, i) => (
              <li key={i}>{c.name}</li>
            ))}
            {comic?.stories?.items?.length == 0 && <li>without stories</li>}
          </ul>
        </div>
      </div>
    </div>
  );
};

export default withRouter(DetailComic);
